#!/bin/bash
# ©2015 Michał Gabor, Wrocław University of Technology
# (matma6@matma6.net)
# ©2014 Jan Kocoń, Wrocław University of Technology
# (jan.kocon@pwr.wroc.pl)
GDZIESKRYPT=`dirname $0`

#create catalog for logs
mkdir logs

#convert premorpt to proper format
find . -type f -name "*.xml" -not -name "header.xml" -print0 | xargs -0 -P 10 -I{} $GDZIESKRYPT/orth2makeonion.sh {}
#merge plain texts into one file in Onion-acceptable format
$GDZIESKRYPT/make_onion_single.py > logs/onion_single 2> logs/onion_single.err
#perform de-duplication
onion -sm single.onion > single.dedup.onion > logs/onion 2> logs/onion.err
#expand Onion file into separate documents
$GDZIESKRYPT/make_onion_multi.py > logs/onion_multi 2> logs/onion_multi.err
#filter too short and with a lot of unknown tokens (see config.py)
$GDZIESKRYPT/filter_alien.py > rejected_list.txt
#tag the rest of files
find dedup -type f -name "*.xces" -print0 | xargs -0 -I{} mkdir {}.dir
#find dedup -type f -name "*.xces" -print0 | xargs -0 -P 10 -I{} wcrft-app nkjp_s2.ini -d /media/dominik/matma6/model_nkjp10_wcrft_s2/ -i xces -o xces -O {}.dir/morph.xml {}
find dedup -type f -name "*.xces" -print0 | xargs -0 -P 10 -I{} wcrft-app nkjp_e2.ini -i xces -o xces -O {}.dir/morph.xml {}
#add header.xml
find . -type d -name '*.dir' | xargs -I{} -n 1 cp $GDZIESKRYPT/header.xml {}
#and some files for poliqarp
#cp $GDZIESKRYPT/nazwa_korpusu.meta.cfg $1.meta.cfg
#cp $GDZIESKRYPT/nazwa_korpusu.meta.lisp $1.meta.lisp
#cp $GDZIESKRYPT/nazwa_korpusu_nkjp.cfg $1.cfg
cp $GDZIESKRYPT/nkjp-tagset.cfg $1.cfg
cp $GDZIESKRYPT/bp.conf $1.bp.conf
#run poliqarp an pray
#bp -n $1 -m morph.xml -e header.xml > logs/bp 2> logs/bp.err
bpng -j $1 # -m pre_morph.xml.tag.xces -e header.xml
#let's clean poliqarp garbage
#find -name ".partial*" | xargs rm
#or not...
