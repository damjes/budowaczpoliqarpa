#!/bin/bash

echo $1

cat $1 | maca-analyse morfeusz-nkjp-official -i premorph -o orth,actual_ws,end_nl | awk '/.+/ { print "<p> " $0 }' > $1.plain.txt
