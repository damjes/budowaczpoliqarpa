#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ©2014 Jan Kocoń, Wrocław University of Technology
# (jan.kocon@pwr.wroc.pl)

import codecs
import os, fnmatch
import re, sys
from collections import defaultdict

def find_files(directory, pattern):
    for root, dirs, files in os.walk(directory):
        for basename in files:
            if fnmatch.fnmatch(basename, pattern):
                filename = os.path.join(root, basename)
                yield filename

i = 0
f_single = codecs.open("single.onion",'wb','utf-8')
for filename in find_files('copy5', '*.plain.txt'):
    i += 1
    if i % 10000 == 0:
        sys.stderr.write("\rDocs processed: %d" % (i) )
        sys.stderr.flush()
    with codecs.open(filename, 'rb', 'utf-8') as content_file:
        fname = filename.decode('utf-8').split("/",3)               
        doc_id = u"<doc id=\"%s\" name=\"%s#%s#%s\">\n<p>\n" % (i, i, fname[2], re.sub(u'[^0-9a-zA-Z]+', '_', fname[3]))
        content = content_file.read()[4:].replace(" ", "\n").replace("<p>\n", "</p>\n<p>\n")
        f_single.write(u"%s%s</p>\n</doc>\n" % (doc_id, content))
f_single.close()
sys.stderr.write("\rDocs processed: %d" % (i) )
sys.stderr.flush()


