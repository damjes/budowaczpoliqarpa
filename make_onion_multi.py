#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ©2014 Jan Kocoń, Wrocław University of Technology
# (jan.kocon@pwr.wroc.pl)

import codecs, os, re, sys
import htmlentitydefs

##
# Removes HTML or XML character references and entities from a text string.
#
# @param text The HTML (or XML) source text.
# @return The plain text, as a Unicode string, if necessary.
def unescape(text):
    def fixup(m):
        text = m.group(0)
        if text[:2] == "&#":
            # character reference
            try:
                if text[:3] == "&#x":
                    return unichr(int(text[3:-1], 16))
                else:
                    return unichr(int(text[2:-1]))
            except ValueError:
                pass
        else:
            # named entity
            try:
                text = unichr(htmlentitydefs.name2codepoint[text[1:-1]])
            except KeyError:
                pass
        return text # leave as is
    return re.sub("&#?\w+;", fixup, text)

try:
    os.stat('dedup')
except:
    os.mkdir('dedup')

doc_description = re.compile(r"<doc id=\"(?P<id>[0-9]+)\" name=\"[0-9]+#(?P<dir>[0-9A-Za-z\-\_\.]+)#(?P<file>[0-9A-Za-z\-\_]+)\">", re.UNICODE)
f_single = codecs.open("single.dedup.onion",'rb','utf-8')
f_current = None
d_current = None
id_current = None
c_current = ""
i = 0
for line in f_single:
    l = line[:-1]
    print l
    if l[:4] == "<doc":
        match = doc_description.search(l)
        f_current = match.group('file')
        d_current = match.group('dir')
        id_current = match.group('id')
    elif l == "</doc>":
        try:
            os.stat("dedup/%s" % d_current)
        except:
            os.mkdir("dedup/%s" % d_current)
        f_name = "dedup/%s/%s_%s" % (d_current, id_current, f_current) 
        f_name = "%s_plain_txt" % f_name[:-10][:200]
        f = codecs.open(f_name, "wb", "utf-8")
        f.write(c_current[:-2])
        f.close()
        c_current = ""
        i += 1
        if i % 10000 == 0:
            sys.stderr.write("\rDocs processed: %d" % (i) )
            sys.stderr.flush()
    elif l == "</p>":
        c_current = "%s\n\n" % c_current[:-1] 
    elif l != "<p>":
        unesc = unescape(unescape(l))
        c_current += "%s " % unesc

f_single.close()
sys.stderr.write("\rDocs processed: %d" % (i) )
sys.stderr.flush()


