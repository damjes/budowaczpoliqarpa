#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ©2014 Jan Kocoń, Wrocław University of Technology
# (jan.kocon@pwr.wroc.pl)
import corpus2

TAGSET = corpus2.get_named_tagset('nkjp')
UNK_MASK = TAGSET.make_ign_tag()
TRESHOLD = 0.2
MINIMUM_TOKENS = 0
MINIMUM_SYMBOLS = 300 #only significant: ~sum([len(token) for token in document])
PROCESSES = 10
MORFEUSZ_CFG = 'morfeusz-nkjp-official'
