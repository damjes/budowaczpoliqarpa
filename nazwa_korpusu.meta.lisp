; Yup, metadata specified as lisp script
; Description:
;   single -> search multiple paths, accept first value that matches and don't collect any more values from given file
;   multi  -> search multiple paths, accept first value that matches as well as any more values along the path of first match
;   date   -> same as single but interpret the result and store it as date instead of text
;   alias  -> after parsing assume first non-empty value

;; (date	"xml.created"	"/cesHeader:date.created")			; Chunk's create date
;; (date	"xml.updated"	"/cesHeader:date.updated")			; Chunk's update date
;; (single	"xml.createor"	"/cesHeader:creator")				; Chunk's creator

; Real data follows

(multi	"autor"
        "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblStruct/analytic/h.author"
        "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblStruct/monogr/h.author")
(single	"tytuł"
        "/cesHeader/fileDesc/sourceDesc/biblFull/titleStmt/h.title"
        "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblStruct/analytic/h.title"
        "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblStruct/monogr/h.title")
(single	"wydawca"
        "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblStruct/monogr/imprint/publisher"
;;         "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblFull/publicationStmt/distributor"
        )
(single	"miejsce_wydania"
;;         "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblFull/publicationStmt/pubAddress"
        "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblStruct/monogr/imprint/pubPlace")
(date "data_wydania"
        "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblFull/publicationStmt/pubDate:value"
        "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblFull/publicationStmt/pubDate"
        "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblStruct/monogr/imprint/pubDate:value"
        "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblStruct/monogr/imprint/pubDate"
;;         "/cesHeader/fileDesc/(sourceDesc/biblFull/)*publicationStmt/pubDate:value"
;;         "/cesHeader/fileDesc/(sourceDesc/biblFull/)*publicationStmt/pubDate")
)
(date "data_pierwszego_wydania"
        "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblStruct/analytic/origDate/firstPubDate:value"
        "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblStruct/monogr/imprint/origDate/firstPubDate:value"
        )
(date "data_powstania"
        "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblStruct/analytic/origDate/createDate:value"
        "/cesHeader/fileDesc/(sourceDesc/biblFull/)*sourceDesc/biblStruct/monogr/imprint/origDate/createDate:value"
        )
(multi	"styl"
        "/cesHeader/profileDesc/textClass/h.keywords/keyTerm")
(single	"medium"
        "/cesHeader/profileDesc/textDesc/channel")



