#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ©2014 Jan Kocoń, Wrocław University of Technology
# (jan.kocon@pwr.wroc.pl)

import codecs
import os, fnmatch
import re, sys
from collections import defaultdict
import corpus2
from wcrft import corpio 
import ctypes, sys
from multiprocessing import Pool
import config

def is_unknown(tok, unk_mask):
    """Return whether the token contains an "uknown word" tag."""
    # mask each tag with the unknown tag, get sum of that
    tok_unkmask = corpus2.mask_token(tok, unk_mask, False)
    # if masked value equals to unk_mask, we've got an unk tag there
    return tok_unkmask == unk_mask


def sentences(reader):
    """Yields subsequent sentences from a reader."""
    while True:
        sentence = reader.get_next_sentence()
        if not sentence:
            break
        yield sentence

def find_files(directory, pattern):
    for root, dirs, files in os.walk(directory):
        for basename in files:
            if fnmatch.fnmatch(basename, pattern):
                filename = os.path.join(root, basename)
                yield filename

def filter_files(filename):
    tokens = 0
    unk_tokens = 0
    symbols = 0
    reader = corpio.get_reader(filename, config.TAGSET, 'text', config.MORFEUSZ_CFG)
    chunks = list()
    while True:
        par = reader.get_next_chunk() # here `chunk' denotes paragraph
        if not par:
            break # end of input
        chunks.append(par)
        for sent in par.sentences():
            for tok in sent.tokens():
                tokens += 1
                symbols += len(tok.orth_utf8())
                if is_unknown(tok, config.UNK_MASK):
                    unk_tokens += 1
    if ((tokens > 0 and (unk_tokens / float(tokens) > config.TRESHOLD)) or 
        tokens < config.MINIMUM_TOKENS or
        symbols < config.MINIMUM_SYMBOLS):
        return (1, filename)
    writer = corpio.get_writer("%s.xces" % filename, config.TAGSET, 'xces')
    for c in chunks:
        writer.write_chunk(c)
    writer.finish()
    return (0, filename)

if __name__ == '__main__':
    p = Pool(config.PROCESSES)
    all_files = 0
    rejected_files = 0
    for (rejected, filename) in p.imap(filter_files, find_files('dedup','*_plain_txt')):
        all_files += 1
        if rejected:
            rejected_files += 1
        if all_files % 1000 == 0:
            sys.stderr.write("\rProcessed: %d Rejected: %d" % (all_files, rejected_files))
            sys.stderr.flush()
        print "%s:%s" % (rejected, filename)
    sys.stderr.write("\rProcessed: %d Rejected: %d" % (all_files, rejected_files))
    sys.stderr.write("\n")
    sys.stderr.flush()
